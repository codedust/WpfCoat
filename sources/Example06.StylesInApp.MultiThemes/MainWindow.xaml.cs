﻿// WpfCoat
// Copyright (C) 2018 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Windows;

namespace Example03b.StylesInApp.DifferentFiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void ButtonTheme1_Click(object sender, RoutedEventArgs e)
        {
            DemoWindow demoWindow = new DemoWindow();

            Uri dictUri = new Uri(@"/Themes/Theme1.xaml", UriKind.Relative);
            ResourceDictionary resourceDict = Application.LoadComponent(dictUri) as ResourceDictionary;
            demoWindow.Resources.MergedDictionaries.Clear();
            demoWindow.Resources.MergedDictionaries.Add(resourceDict);

            demoWindow.Show();
        }
        
        private void ButtonTheme2_Click(object sender, RoutedEventArgs e)
        {
            DemoWindow demoWindow = new DemoWindow();

            Uri dictUri = new Uri(@"/Themes/Theme2.xaml", UriKind.Relative);
            ResourceDictionary resourceDict = Application.LoadComponent(dictUri) as ResourceDictionary;
            demoWindow.Resources.MergedDictionaries.Clear();
            demoWindow.Resources.MergedDictionaries.Add(resourceDict);

            demoWindow.Show();
        }
    }
}